let count = 1;
while(count <= 10) {
	console.log(`count is ${count}`);
	count += 2;
};

let num = -10;
while(num < 20) {
	console.log(num);
	num++;
};

let num2 = 10;
while(num2 <= 40) {
	console.log(num2);
	num2 += 2;
}

let odd = 301;
while(odd <= 333) {
	if(odd%2 != 0) {
		console.log(odd);
	};
	odd++;
};